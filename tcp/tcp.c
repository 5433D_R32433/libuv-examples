#include <uv.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>



int main ( int argc, char **argv, char **envp )
{
	uv_loop_t loop = { 0 }; // malloc(sizeof(uv_loop_t));
    uv_loop_init  ( &loop );
    printf        ( "Hello, World!\n"     );
    uv_run        ( &loop, UV_RUN_DEFAULT );
    uv_loop_close ( &loop );
	return 0;
}